FROM alpine:20200319
LABEL maintainer="Richard Kjerstadius"
LABEL description="Minimalist image with Clang and Python 3."

RUN apk --no-cache add \
    python3 \
    clang \
    clang-extra-tools \
    git

# Create a python symbolic link in order to please git-clang-format
RUN ln -s /usr/bin/python3 /usr/bin/python
